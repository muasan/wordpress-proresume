<?php 

define('PRORESUME_BASE_URI', get_template_directory_uri());
define('PRORESUME_BASE_PATH', get_template_directory());

include_once('core/helpers.php');
include_once('core/custom-post-type.php');
include_once('core/hooks.php');
include_once('core/theme-setting.php');
include_once('core/blog.php');
include_once('core/home.php');
include_once('core/work.php');
include_once('core/general.php');





function proresume_required_css() {
	wp_register_style('bootstrap', PRORESUME_BASE_URI. '/assets/bootstrap/css/bootstrap.min.css');
	wp_register_style('font', PRORESUME_BASE_URI. '/assets/css/font-awesome.min.css', array(), false, "screen");
	wp_register_style('animate', PRORESUME_BASE_URI. '/assets/css/animate.css');
	wp_register_style('magnific', PRORESUME_BASE_URI. '/assets/css/magnific-popup.css');
	wp_register_style('custom-style', PRORESUME_BASE_URI. '/assets/css/style.css', array(), false, "screen");
	wp_register_style('responsive', PRORESUME_BASE_URI. '/assets/css/responsive.css');
	wp_register_style('symbols', PRORESUME_BASE_URI. '/assets/css/symbols.css');
	wp_register_style('slideme-css', PRORESUME_BASE_URI. '/assets/css/slideme.css');
	wp_register_style('footer', PRORESUME_BASE_URI. '/assets/css/footer.css');
	wp_register_style('scroller', PRORESUME_BASE_URI. '/assets/css/scroller.css');

	wp_register_style('home', PRORESUME_BASE_URI. '/assets/css/home.css');


	wp_register_style('blog-css', PRORESUME_BASE_URI . '/assets/css/blog.css');

}

function proresume_required_css_admin() {
	wp_enqueue_style('my-admin-parallax', PRORESUME_BASE_URI . '/assets/css/admin-style.css');

}

function proresume_required_scripts_admin() {
	wp_enqueue_script('jquery-ui', PRORESUME_BASE_URI . '/assets/js/jquery-ui.min.js', array(), false, false);

	wp_enqueue_script('jquery', PRORESUME_BASE_URI . '/assets/js/jquery.js', array(), false, false);
	

}

function proresume_customizer_scripts () {
	wp_enqueue_script('parallax_one_customizer', PRORESUME_BASE_URI . '/assets/js/parallax_one_customizer.js', array(), false, true);
	wp_enqueue_media();	
	wp_enqueue_script('proresume_customize_file_control', proresume_get_asset_url('js/proresume_customize_file_control.js'), array('jquery'), true, true);

	wp_localize_script( 'proresume_customizer_scripts', 'parallaxOneCustomizerObject', array(
		
		'documentation' => esc_html__( 'Documentation', 'parallax-one' ),
		'support' => esc_html__('Support Forum','parallax-one'),
		'pro' => __('Upgrade to PRO','parallax-one'),
		
	) );
}

function proresume_required_scripts() {
	wp_deregister_script('jquery');

	wp_enqueue_script('jquery', PRORESUME_BASE_URI . '/assets/js/jquery.js', array(), false, false);
	wp_enqueue_script('jquery.stellar', PRORESUME_BASE_URI . '/assets/js/jquery.stellar.min.js', array(), false, true);
	wp_enqueue_script('jquery.stiky', PRORESUME_BASE_URI . '/assets/js/jquery.sticky.js', array(), false, true);
	wp_enqueue_script('jquery.counto', PRORESUME_BASE_URI . '/assets/js/jquery.countTo.js', array(), false, true);
	wp_enqueue_script('jquery.inview', PRORESUME_BASE_URI . '/assets/js/jquery.inview.min.js', array(), false, true);
	wp_enqueue_script('jquery.easypiechart', PRORESUME_BASE_URI . '/assets/js/jquery.easypiechart.js', array(), false, true);
	wp_enqueue_script('jquery.shuffle', PRORESUME_BASE_URI . '/assets/js/jquery.shuffle.min.js', array(), false, true);
	wp_enqueue_script('jquery.magnific-popup', PRORESUME_BASE_URI . '/assets/js/jquery.magnific-popup.min.js', array(), false, true);
	wp_enqueue_script('jquery.fitvids', PRORESUME_BASE_URI . '/assets/js/jquery.fitvids.js', array(), false, true);
	wp_enqueue_script('bootstrap', PRORESUME_BASE_URI . '/assets/bootstrap/js/bootstrap.min.js', array(), false, true);
	wp_enqueue_script('smoothscroll', PRORESUME_BASE_URI . '/assets/js/smoothscroll.js', array(), false, true);
	wp_enqueue_script('wow', PRORESUME_BASE_URI . '/assets/js/wow.min.js', array(), false, true);
	wp_enqueue_script('external-froogaloop2', 'http://a.vimeocdn.com/js/froogaloop2.min.js', array(), false, true);	
	wp_enqueue_script('slideme', PRORESUME_BASE_URI . '/assets/js/jquery.slideme2.js', array(), false, true);
	wp_enqueue_script('myscript', PRORESUME_BASE_URI . '/assets/js/scripts.js', array(), false, true);
	wp_enqueue_script('img-lazy-loading', PRORESUME_BASE_URI . '/assets/js/img-lazy-loading.js', array(), false, true);

	wp_register_script('prettify', 'https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?skin=desert', array(), false, true);
}



function proresume_get_asset($file_path) {
	$ouput_file = join('/', array(PRORESUME_BASE_PATH, 'assert', $file_path));

	return file_exists($ouput_file) ? $ouput_file : FALSE;

}

function proresume_get_asset_url($path) {
	return PRORESUME_BASE_URI . "/assets/{$path}";
}
?>