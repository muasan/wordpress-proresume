<?php 

$args = array(
          'post_type' => 'skill',
          'meta_key' => 'is_main',
          'orderby' => 'meta_value_num'
        );

$query = new WP_Query($args);

if ($query->have_posts()) :

$main_skills = array();
$other_skills = array();
proresume_extract_skills_by_type($query, $main_skills, $other_skills);
?>

<!-- Skills Section -->
<section id="skills" class="skills-section section-padding">
  <div class="container">
    <h2 class="section-title wow fadeInUp">Skills</h2>

    <div class="row">
      <?php foreach ($main_skills as $skill) : $rate = get_field('cover', $skill->ID); ?>
      <div class="col-md-6">
        <div class="skill-progress">
          <div class="skill-title"><h3><?php echo $skill->post_title; ?></h3></div> 
          <div class="progress">
            <div class="progress-bar six-sec-ease-in-out" role="progressbar" aria-valuenow="<?php echo $rate; ?>" aria-valuemin="0" aria-valuemax="100" ><span><?php echo $rate; ?>%</span>
            </div>
          </div><!-- /.progress -->
        </div><!-- /.skill-progress -->

      </div><!-- /.col-md-6 -->
      <?php endforeach;?>
    </div><!-- /.row -->

    <?php if (count($other_skills) > 0) : ?>
    <div class="skill-chart text-center">
      <h3>More skills</h3>
    </div>
      
    <div class="row more-skill text-center">
      <?php foreach($other_skills as $skill) : $rate = get_field('cover', $skill->ID); ?>
      <div class="col-xs-12 col-sm-4 col-md-2">
          <div class="chart" data-percent="<?php echo $rate; ?>" data-color="e74c3c">
                <span class="percent"></span>
                <div class="chart-text">
                  <span><?php echo $skill->post_title; ?></span>
                </div>
            </div>
      </div>
      <?php endforeach;?>

    </div>
    <?php endif;?>

  </div><!-- /.container -->
</section><!-- End Skills Section -->
<?php endif; ?>