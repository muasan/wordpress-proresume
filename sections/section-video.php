<?php 
  $introducing = get_theme_mod('proresume_video_section_introduction', 'Video introduction');
  $modal_title = get_theme_mod('proresume_video_section_title', 'Video Tour');
  $links = get_theme_mod('proresume_video_section_links', '[]');
  $autoplay = get_theme_mod('proresume_video_section_autoplay', true);
  $background = get_theme_mod('proresume_video_section_background', false);
  if (!$background || $background === '')
    $background = proresume_get_asset_url('images/video-bg.jpg');


  $links = json_decode($links);
  $links_tmp = array();
  if ($autoplay) {
    foreach ($links as $key => $link) {
      if (array_key_exists($key, $links_tmp))
        $links_tmp[$key] = $link;

      $links[$key]->autoplay = true;
    }
    //$links = $links_tmp;
  }

  $first_link = count($links) > 0 ? $links[0]->link : 'http://player.vimeo.com/video/118119037';
  $links = json_encode($links);

?>

<!-- Video Section -->
<section id="video" class="video-section">
  <div class="tf-bg-overlay">
    <div class="container">
      <div class="control">
        <div class="video-intro text-center">
          <button type="button" class="play-trigger" data-toggle="modal" data-target="#tour-video"><i class="fa fa-play"></i></button>
          <h2>Video Introducing</h2>
        </div>
        
        <!-- Video Modal -->
        <div class="modal modal-video" id="tour-video">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Video Tour</h4>
              </div>
              <div class="modal-body">
                <div class="arrow-left"></div>
                <div class="arrow-right"></div>
                <div class="video-container">
                  <iframe id="vimeo-iframe"  src="<?php echo $first_link; ?>" name="vimeoplayer" id="nofocusvideo" width="500" height="281" ></iframe>
                </div><!--/.video-container-->

              
              </div>
              <!--/.modal-body-->
            </div>
            <!--/.modal-content-->
          </div>
          <!--/.modal-dialog-->
        </div>
        <!--/.modal-->                    
      </div>
      <!--/.control-->
    </div>
    <!--/.container-->
  </div>
  <!--/.overlay-->
</section>
<!-- /.Video Section -->


<script type="text/javascript">
//setup
(function($){
  var data_arr = JSON.parse('<?php echo $links ?>') ;
  var vimeo_iframe = $('#vimeo-iframe');
  var cur = 0;

  //listener model showed up
  $('#tour-video').on('shown.bs.modal', function(){
    linkObj = getNext(0);
    replaceLink(linkObj);
  });

  //listener model hidden
  $('#tour-video').on('hidden.bs.modal', function () {
    vimeo_iframe.attr('src', '');
  })

  //left handle
  $('#video .arrow-left').on('click',function() {
    next = getNext(-1);
    vimeo_iframe.attr('src', next.link);
    replaceLink(next);
  });

  //right handle
  $('#video .arrow-right').on('click',function() {
    next = getNext(1);
    vimeo_iframe.attr('src', next.link);
    replaceLink(next);

  });

  function replaceLink(linkObj) {
    if (linkObj && linkObj.autoplay)
      vimeo_iframe.attr('src', linkObj.link + '?autoplay=1');
  }

  function getNext(sign) {
    if (sign < 0)
      cur = cur == 0 ? data_arr.length - 1 : cur - 1;
    else if (sign > 0)
      cur = cur == data_arr.length - 1 ? 0 : cur + 1;
    

    return data_arr[cur];
  }


})(jQuery);

//set bacbkground image
(function($){
  var ele = $('#video');
  ele.css('background', 'url( <?php echo $background; ?>) no-repeat center center' );
  ele.css('webkit-background-size', 'cover');
  ele.css('background-size', 'cover');

})(jQuery);

</script>



