<?php 
function extract_summary($summary) {
	$lines = explode("\n", $summary);
	$copy_lines = $lines;
	foreach ($lines as $index => $value) {
		$copy_lines[$index] = explode(":", $value);
	}
	return $copy_lines;
}
$gallery = get_field('gallery');
$gallery = ! is_array($gallery) ? array() : $gallery;
$summary = get_field('summary');
$summary = $summary ? $summary : "Author: Nhan\nYear: 2015";
$summary = extract_summary($summary);

$tags = wp_get_object_terms($post->ID, 'work_tag', array('fields' => 'slugs')); 

?>

<section id="single-work" class="single-work">
<?php if (is_array($gallery) && count($gallery) > 0) : ?>
	<div class="gallery">
		<div class="container-fluid">
			<div class="row">
				<div id="galery-slider" class="slideme2Theme">
					<ul class="slideme slideme-zoom">
					<?php foreach ($gallery as $image) : ?>
						<li><img src="<?php echo $image['url'];?>"/></li>
					<?php endforeach;?>
					</ul>
				</div>
			</div>
		</div>
	</div><!-- /gallery -->
<?php endif;?>

	<div class="information">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="content">
						<?php the_title('<h2>', '</h2>'); ?>
						<p><?php echo apply_filters ("the_content", $post->post_content); ?>
						</p>

					</div>

				</div> <!-- /col-md-8 -->

				<div class="col-md-4">
					<div class="summary">

						<ul>
						<?php foreach ($summary as $line) : ?>
							<li><strong><?php echo trim($line[0])?>: </strong><?php echo trim($line[1])?></li>
						<?php endforeach; ?>
							<li> 
							<?php foreach ($tags as $key => $value) {
								echo "<div class='btn btn-primary'>$value</div><span> </span>";
							}?> 
							</li>

						</ul>
					</div>
				</div> <!-- /col-md-4 -->
			</div>
		</div>
	</div><!-- /Information -->
</section>