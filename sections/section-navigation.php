<?php 
	$brand_img_url = get_theme_mod('proresume_nav_section_brand', proresume_get_asset_url('images/logo.png'));
	$brand_img_url = $brand_img_url ? $brand_img_url : proresume_get_asset_url('images/logo.png');
?>
<!-- Navigation -->
	<header class="header">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo $brand_img_url; ?>" alt=""></a>
				</div>

				<?php 
					if (is_home())
						wp_nav_menu(array(
							'container_class' => 'collapse navbar-collapse',
							'container_id' => 'custom-collapse',
							'menu_class' => 'nav navbar-nav navbar-right',
							'menu' => 'primary'
						)); 
					else 
						wp_nav_menu(array(
							'container_class' => 'collapse navbar-collapse',
							'container_id' => 'custom-collapse',
							'menu_class' => 'nav navbar-nav navbar-right',
							'menu' => 'secondary'
						));
				?>

				
			</div><!-- .container -->
		</nav>

	</header><!-- End Navigation -->