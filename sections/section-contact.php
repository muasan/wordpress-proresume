<?php 
  $form_shortcode = get_theme_mod('proresume_contact_section_form_shortcode', false);
  $map_shortcode  = get_theme_mod('proresume_contact_section_map_shortcode', false);
  $address        = get_theme_mod('proresume_contact_section_address', '');
  $phonenumbers   = get_theme_mod('proresume_contact_section_phonenumbers', '');
  $phonenumbers   = explode(';', $phonenumbers);
?>



<!-- Contact Section -->
<section id="contact" class="contact-section section-padding">
  <div class="container">
    <h2 class="section-title wow fadeInUp">Get in touch</h2>

    <div class="row">
      <div class="col-md-6">
        <div class="contact-form">
          <strong>Send me a message</strong>

          <?php 
            if ($form_shortcode)
              echo do_shortcode($form_shortcode); 

          ?>
        </div>
          

      </div><!-- /.col-md-6 -->

      <div class="col-md-6">
        <div class="row center-xs">
          <div class="col-sm-6">
            <i class="fa fa-map-marker"></i>
            <address>
              <strong>Address/Street</strong>
              <?php echo $address; ?>
            </address>
          </div>

          <div class="col-sm-6">
            <i class="fa fa-mobile"></i>
            <div class="contact-number">
              <strong>Phone Number</strong>
              <?php 
                foreach ($phonenumbers as $index => $number) {
                  $number = trim($number);
                  echo "<a href='tel:$number'>$number</a>";
                  if ($index < count($phonenumbers) - 1)
                    echo '<br>';
                }
              ?>
            </div>
          </div>
        </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="location-map">
            <?php if ($map_shortcode) echo do_shortcode($map_shortcode);?>
          </div>
        </div>
      </div>

      </div>
    </div><!-- /.row -->
  </div><!-- /.container -->
</section><!-- End Contact Section -->
