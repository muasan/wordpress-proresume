<?php 
$tags = get_terms('work_tag', array(
          'taxonomy' => 'work_tag',
        ));
$obj = (object) array('name' => 'All', 'slug' => 'all', 'term_id' => 0);
if ($tags) array_unshift($tags, $obj);

$args = array(
          'post_type' => 'work'
        );
$query = new WP_Query($args);

function generateImageData($gallery) {
  if (is_array($gallery))
    $gallery = $gallery[0];

  $ratio = $gallery['height'] != 0 ? $gallery['width']/$gallery['height']: 1;
  $preloader_src = proresume_get_asset_url("images/preloader.gif");
  $result = "data-preloader-src='$preloader_src' data-thumb-src='{$gallery['sizes']['thumbnail']}' data-medium-src='{$gallery['sizes']['medium']}', data-medium-large-src='{$gallery['sizes']['medium_large']}' data-large-src='{$gallery['sizes']['large']}' data-original-src='{$gallery['url']}' data-ratio='$ratio'";
  return $result;
}

?>

<!-- Works Section -->
<section id="works" class="works-section section-padding">
  <div class="container">
    <h2 class="section-title wow fadeInUp">Works</h2>

    <ul class="list-inline" id="filter">
        <?php foreach ($tags as $tag) : $tag = (array)$tag; ?>
        <?php if ($tag['term_id'] == 0) : ?>
        <li><a class="active" data-group="<?php echo $tag['slug']; ?>"><?php echo $tag['name']; ?></a></li>
        <?php else : ?>
        <li><a data-group="<?php echo $tag['slug']; ?>"><?php echo $tag['name']; ?></a></li>
        <?php endif; ?>
        <?php endforeach; ?>
    </ul>

    <div class="row">
      <div id="grid">
        <?php while ($query->have_posts()) : $query->the_post(); ?>
        <?php 
          $tags = wp_get_object_terms($post->ID, 'work_tag', array('fields' => 'slugs')); 
          array_unshift($tags, 'all'); 
          $gallery = get_field('gallery', $query->post->ID);
          $link = get_field('link', $query->post->ID);
          $link = $link === '' ? get_permalink($query->post->ID) : $link;
          $gallery = count($gallery) == 0 ? array(array('url' => proresume_get_asset_url('images/works/default.png'), 'sizes'=> array('medium' => proresume_get_asset_url('images/works/default.png')) )) : $gallery;
          $gallery_json = json_encode($gallery);
        ?>
        <div class="portfolio-item col-xs-12 col-sm-4 col-md-3" data-groups='<?php echo json_encode($tags); ?>'>
          <a href="<?php echo $link; ?>">
            <div class="portfolio-bg">
              <div class="portfolio">
                <div class="tt-overlay"></div>
                <div class="links">
                  <a class="image-link"  href="<?php echo $gallery[0]['url']; ?>"><i class="fa fa-search-plus"></i></a>
                  <a href="<?php echo $link; ?>"><i class="fa fa-link"></i></a>                          
                </div><!-- /.links -->

                <img <?php echo generateImageData($gallery); ?> alt="image"> 
                <div class="portfolio-info">
                  <h3><?php echo $query->post->post_title; ?></h3>
                </div><!-- /.portfolio-info -->
              </div><!-- /.portfolio -->
            </div><!-- /.portfolio-bg -->
          </a><!-- /a -->
        </div><!-- /.portfolio-item -->
        <?php endwhile; ?>


        
      </div><!-- /#grid -->
    </div><!-- /.row -->
  </div><!-- /.container -->
</section><!-- End Works Section -->
