<?php
$title = get_theme_mod('proresume_home_settings_title', 'I am XYZ');
$heading1 = get_theme_mod('proresume_home_settings_heading1', 'H1');
$heading2 = get_theme_mod('proresume_home_settings_heading2', 'h2');
$description = get_theme_mod('proresume_home_settings_description', 'This is description');
$social_icons = get_theme_mod('proresume_home_section_socials', "[]");
$social_icons = json_decode($social_icons);
?>

<!-- Preloader -->
	<div id="tt-preloader">
		<div id="pre-status">
			<div class="preload-placeholder"></div>
		</div>
	</div>

	<!-- Home Section -->
	<section id="home" class="tt-fullHeight" data-stellar-vertical-offset="50" data-stellar-background-ratio="0.2">
		<div class="intro">
			<div class="intro-sub"><?php echo $title ?></div>
			<h1><?php echo $heading1 ?><span><?php echo $heading2?></span></h1>
			<p><?php echo str_replace("\n", "<br/>", $description); ?></p>

      <div class="social-icons">
        <ul class="list-inline">
        	<?php foreach ($social_icons as $index => $value) {
        		echo "<li> <a href='{$value->link}'> <i class='fa {$value->icon_value}'></i> </a> </li>";
        	}?>
          
        </ul>
      </div> <!-- /.social-icons -->
		</div>

		<div class="mouse-icon">
			<div class="wheel"></div>
		</div>
	</section><!-- End Home Section -->