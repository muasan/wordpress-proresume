<?php 
/*
category
[
date
timeline_title
timeline_subtitle
timeline_detail
]
*/
function get_year($date_str) {
    $date = new DateTime($date_str);
    return $date->format('Y');
}

function format($date_str, $format_str) {
    $date = new DateTime($date_str);
    return $date->format($format_str);
}

$cargs = get_categories( array(
        'child_of'      => 0,
        'orderby'       => 'name',
        'order'         => 'ASC',
        'hide_empty'    => 1,
        'taxonomy'      => 'milestone_cat', //change this to any taxonomy
    ));

?>

<!-- Resume Section -->
<section id="resume" class="resume-section section-padding">
    <div class="container">
        <h2 class="section-title wow fadeInUp">Resume</h2>
        <?php 
            foreach ($cargs as $cat) :
            $args = array(
                'post_type' => 'milestone',
                'meta_key' => 'start_date',
                'orderby' => 'meta_value_num',
                'order' => 'ASC',
                'tax_query' => array( array(
                    'taxonomy' => 'milestone_cat',
                    'field' => 'slug',
                    'terms' => $cat->slug
                )),
                'posts_per_page' => -1
            );

            $query = new WP_Query($args);
            if (!$query->have_posts())
                continue;
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="resume-title">
                    <h3><?php echo $cat->name; ?></h3>
                </div>
                <div class="resume">
                    <ul class="timeline">
                        <?php 
                        while ($query->have_posts()) : 
                            //$query->next_post_link();
                            //$post = $query->post; 
                            $query->the_post();
                            $post = $query->post;
                            $start_date = get_field('start_date', $post->ID, false);
                            $end_date = get_field('end_date', $post->ID, false);
                            $title = $post->post_title;
                            $sub_title = get_field('sub_title', $post->ID);
                            $detail = get_field('detail', $post->ID);

                        ?>

                        <li class="<?php if ($query->current_post%2==1) echo 'timeline-inverted'; ?>">
                            <div class="posted-date">
                                <span class="month">
                                    <?php 
                                        if (get_year($start_date) == get_year($end_date))
                                            echo format($start_date, 'M') . '-' . format($end_date, 'M') . ' ' .get_year($start_date);
                                        else
                                            echo get_year($start_date) . '-' . get_year($end_date); 
                                    ?>
                                </span>
                            </div><!-- /posted-date -->

                            <div class="timeline-panel wow fadeInUp">
                                <div class="timeline-content">
                                    <div class="timeline-heading">
                                        <h3><?php echo esc_html($title); ?></h3>
                                        <span><?php echo esc_html($sub_title); ?></span>
                                    </div><!-- /timeline-heading -->

                                    <div class="timeline-body">
                                        <p><?php echo ($detail); ?></p>
                                    </div><!-- /timeline-body -->
                                </div> <!-- /timeline-content -->
                            </div><!-- /timeline-panel -->
                        </li>
                        <?php endwhile; ?>

                        
                    </ul>
                </div>
            </div>
        </div><!-- /row -->


        <?php endforeach; ?>
        </div><!-- /row -->
    </div><!-- /.container -->
</section><!-- End Resume Section -->