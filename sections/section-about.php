<?php 

$personal_infos = json_decode( get_theme_mod('proresume_about_section_infomation', '[]') );
$photo = get_theme_mod('proresume_about_section_avata', proresume_get_asset_url('images/myphoto.jpg'));
$intros = json_decode( get_theme_mod('proresume_about_section_introductions', '[]') );
$signature = get_theme_mod('proresume_about_section_signature', proresume_get_asset_url('images/sign.png'));
$cv_url = get_theme_mod('proresume_about_section_cv', '#');

?>

<!-- About Section -->
<section id="about" class="about-section section-padding">
  <div class="container">
    <h2 class="section-title wow fadeInUp">About Me</h2>

    <div class="row">

      <div class="col-md-4 col-md-push-8">
        <div class="biography">
          <div class="myphoto">
            <img src="<?php echo $photo; ?>" alt="">
          </div>
          <ul>
            <?php foreach ($personal_infos as $key => $value) {
              echo "<li><strong>{$value->title}:</strong> {$value->text}</li>";
            }?>

          </ul>
        </div>
      </div> <!-- col-md-4 -->

      <div class="col-md-8 col-md-pull-4">
        <?php foreach ($intros as $key => $value): ?>
          <div class="short-info wow fadeInUp">
            <h3><?php echo $value->title; ?></h3>
            <p><?php echo $value->text; ?></p>

            <ul class="list-check">
              <?php
              $list_checks = explode(';', html_entity_decode(trim($value->subtitle)));
              foreach ($list_checks as $key => $check) :
                $check = trim($check);
                if ($check != '' )
                  echo "<li>{$check}</li>";
              endforeach;
              ?>
            </ul>
          </div>
        <?php endforeach; ?>
        

        <div class="my-signature">
          <img src="<?php echo $signature; ?>" alt="">
        </div>

        <div class="download-button">
          <a class="btn btn-info btn-lg" href="#contact"><i class="fa fa-paper-plane"></i>Send me message</a>
          <a class="btn btn-primary btn-lg" href="<?php echo $cv_url; ?>" download><i class="fa fa-download"></i>download my cv</a>
        </div>
      </div>


    </div> <!-- /.row -->
  </div> <!-- /.container -->
</section><!-- End About Section -->