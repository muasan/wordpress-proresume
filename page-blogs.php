<?php
/**
 * Template Name: Blogs
 */
?>

<?php get_header(); ?>
<div class='container-fluid'>
	<div class='row'>
<?php get_sidebar('blog'); ?>
<?php 
	$args = array('post_type'=>'post');
	$the_query = new WP_Query( $args ); 
?>

<article id='home' class='col-sm-12 col-md-9 col-md-offset-3 col-lg-6 col-lg-offset-3 content'>
	<div class='entries'>
	<?php if ($the_query->have_posts()) : ?>
		<? while ($the_query->have_posts()) : $the_query->next_post(); $the_post = $the_query->post; ?>
		<section class='post-entry'>
			<a href='<?php echo get_permalink($the_post); ?>'>
				<div class='post-date'><?php echo get_the_date('F j, Y', $the_post)?></div>
				<div class='post-title'><?php echo $the_post->post_title; ?></div>
			</a>
			

		</section>
	<? endwhile; ?>
	<?php else : //No posts found?>
		No posts found
	<?php endif;?>
</div>
</article>
	</div>
</div>
<?php get_footer(); ?>


