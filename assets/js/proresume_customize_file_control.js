
jQuery(document).ready(function($){
 
    var custom_uploader;
 
    //console.log('Iam here');
    $(".upload_file_button").click(function(e) {
 
        var control_id =  $(this).data('id'); 

        e.preventDefault();

        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
 
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose File',
            button: {
                text: 'Choose File'
            },
            multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();

            textbox = $('.upload_file[data-id=' + control_id + ']');
            textbox.val(attachment.url);
            textbox.trigger('change');
        });
 
        //Open the uploader dialog
        custom_uploader.open();
 
    });
 
 
});