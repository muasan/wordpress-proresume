//Support lazy loading for images

(function(){
try{
	var imgs = document.querySelectorAll("img[data-thumb-src]");

	for (var i = 0, img = imgs[i]; i < imgs.length; i++, img = imgs[i] ){
		var srcUrl = "";

		if (img.parentNode.offsetWidth <= 150)
			srcUrl = img.dataset.thumbSrc;
		else if (img.parentNode.offsetWidth <= 300)
			srcUrl = img.dataset.mediumSrc;
		else if (img.parentNode.offsetWidth <= 768)
			srcUrl = img.dataset.mediumLargeSrc;
		else if (img.parentNode.offsetWidth <= 1024)
			srcUrl = img.dataset.largeSrc;
		else
			srcUrl= img.dataset.originalSrc;

		var preloaderEle = document.createElement('img');
		img._preloaderEle = preloaderEle;
		img.parentNode.style['height'] = 1/parseFloat(img.dataset.ratio) * img.parentNode.offsetWidth + 'px';
		preloaderEle.classList.add("img-preloader");
		preloaderEle.setAttribute('src', img.dataset.preloaderSrc);
		img.parentNode.appendChild(preloaderEle);

		img.onload = onImageloaded;
		img.setAttribute('src', srcUrl);
	}
	function onImageloaded(e) {
		this.parentNode.removeChild(this._preloaderEle);
		this.parentNode.style['height'] = 'auto';

		this.classList.add("loaded");
	}

}//end try
catch(e) {
	console.error(e);
}
})();