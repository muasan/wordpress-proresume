<?php 
/**
 * This is a template to represent for posts
 * Note: PHP codes and HTML will be mixed. PHP codes being used for binding data
 *
 */
?>
<?php get_header(); ?>
<div class='container-fluid'>
	<div class='row'>
<?php get_sidebar('blog'); ?>
<?php 
	the_post(); 
	$next_post = get_next_post();

?>
<article id='home' class='col-sm-12 col-md-9 col-md-offset-3 col-lg-6 col-lg-offset-3 content'>
	<header>
		<h1 class='post-title'><?php echo the_title(); ?></h1>
		<div class='post-date'><?php echo get_the_modified_time('F j, Y h:ia'); ?></div>
	</header>
	<section class='post-content'>
		<?php echo the_content(); ?>
	</section>

	<?php if ($next_post) : ?>
	<section class='next-post'>
		<a href='<?php echo get_permalink($next_post); ?>'>
			<i class="fa fa-angle-double-right fa-2x next-post-icon" aria-hidden="true"></i>
			<h3 class='next-post-title'> <?php echo $next_post->post_title;?> </h3>
		</a>
	</section>
<?php endif; ?>

<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

var disqus_config = function () {
this.page.url = '<?php echo get_permalink(); ?>';  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = '<?php echo get_permalink(); ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://nhanbach-com.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

</article>
	</div>
</div>
<?php get_footer(); ?>

