<?php 
include_once (PRORESUME_BASE_PATH . '/core/class/parallax-one-general-control.php');
include_once (PRORESUME_BASE_PATH . '/core/parallax.php');
include_once (PRORESUME_BASE_PATH . '/core/class/proresume_customize_file_control.php');

function proresume_customize_register($wp_customize) {

	/****************************************/
	/* HOME SECTION */
	/****************************************/
	$wp_customize->add_section( 
		'proresume_home_section' , 
		array(
    'title'      => __( 'Home section', 'proresume' ),
    'priority'   => 30,
	) );

	/* Title */
	$wp_customize->add_setting(
			'proresume_home_settings_title', 
			array(
				'default' => 'XYZ',
				'sanitize_callback' => 'sanitize_text_field'
			)
	);
	$wp_customize->add_control(
			'proresume_home_settings_title', 
			array(
				'label' => __('Nickname', 'proresume'),
				'section' => 'proresume_home_section',
				'type' => 'text'
			)
	);

	/* Heading 1 */
	$wp_customize->add_setting(
			'proresume_home_settings_heading1', 
			array(
				'default' => 'Heading1',
				'sanitize_callback' => 'sanitize_text_field'
			)
	);
	$wp_customize->add_control(
			'proresume_home_settings_heading1', 
			array(
				'label' => __('Heading 1', 'proresume'),
				'section' => 'proresume_home_section',
				'type' => 'text'
			)
	);

	/* Heading 2 */
	$wp_customize->add_setting(
			'proresume_home_settings_heading2', 
			array(
				'default' => 'Heading2',
				'sanitize_callback' => 'sanitize_text_field'
			)
	);
	$wp_customize->add_control(
			'proresume_home_settings_heading2', 
			array(
				'label' => __('Heading 2', 'proresume'),
				'section' => 'proresume_home_section',
				'type' => 'text'
			)
	);

	/* Description */
	$wp_customize->add_setting(
		'proresume_home_settings_description', 
		array(
			'default' => 'This is a desciption',
			'sanitize_callback' => 'esc_html'
		)
	);
	$wp_customize->add_control(
    new WP_Customize_Control(
        $wp_customize,
        'proresume_home_settings_description',
        array(
            'label'          => __( 'Description', 'theme_name' ),
            'section'        => 'proresume_home_section',
            'settings'       => 'proresume_home_settings_description',
            'type'           => 'textarea',
            
        )
    )
	);

	/* Socials */
	$wp_customize->add_setting(
		'proresume_home_section_socials',
		array(
			'sanitize_callback' => 'parallax_one_sanitize_repeater',
			'default' => json_encode(
				array(
					array('icon_value' => 'fa-facebook', 'link' => '#'),
					array('icon_value' => 'fa-twitter', 'link' => '#'),
				)
			)
		)
	);

	$wp_customize->add_control( new Parallax_One_General_Repeater( $wp_customize, 'proresume_home_section_socials', array(
		'label'   => esc_html__('Add new social icon','parallax-one'),
		'section' => 'proresume_home_section',
		'active_callback' => 'parallax_one_show_on_front',
		'priority' => 10,
        'parallax_image_control' => false,
        'parallax_icon_control' => true,
        'parallax_text_control' => false,
        'parallax_link_control' => true

	) ) );


	/****************************************/
	/* NAVIGATION SECTION */
	/****************************************/
	$wp_customize->add_setting(
		'proresume_nav_section_brand',
		array(
			'sanitize_callback' => 'esc_url',
			'default' => PRORESUME_BASE_URI . '/assets/images/logo.png'
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'proresume_nav_section_brand',
			array(
				'label' => __('Brand Image', 'proresume'),
				'section' => 'title_tagline'
			)
		)
	);


	/****************************************/
	/* ABOUT SECTION */
	/****************************************/
	$wp_customize->add_section( 
		'proresume_about_section' , 
		array(
    'title'      => __( 'About section', 'proresume' ),
    'priority'   => 30,
	) );

	/* Profile */
	$wp_customize->add_setting(
		'proresume_about_section_infomation',
		array(
			'sanitize_callback' => 'parallax_one_sanitize_repeater',
			'default' => json_encode(array(
				array('title' => 'Name', 'text' => 'Nhan Bach'),
				array('title' => 'Date of birth', 'text' => '05 Dec 1993'),
				array('title' => 'Address', 'text' => '239/2 Awesome Street, USA'),
				array('title' => 'Nationality', 'text' => 'American'),
				array('title' => 'Phone', 'text' => '(000) 1234 56789'),
				array('title' => 'Email', 'text' => 'yourmail@iamx.com'),

			))
		)
	);

	$wp_customize->add_control( new Parallax_One_General_Repeater( $wp_customize, 'proresume_about_section_infomation', array(
		'label'   => esc_html__('Personal information','proresume'),
		'section' => 'proresume_about_section',
		'priority' => 10,
        'parallax_title_control' => true,
        'parallax_text_control' => true,
	) ) );

	/* avata */
	$wp_customize->add_setting(
		'proresume_about_section_avata',
		array(
			'sanitize_callback' => 'esc_url',
			'default' => proresume_get_asset_url('images/myphoto.jpg')
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'proresume_about_section_avata',
			array(
				'label' => __('Your photo', 'proresume'),
				'section' => 'proresume_about_section',
			)
		)
	);

	/* Introduce */
	$wp_customize->add_setting(
		'proresume_about_section_introductions',
		array(
			'sanitize_callback' => 'parallax_one_sanitize_repeater',
			'default' => json_encode(array(
				array('title' => 'objective', 'subtitle' => '', 'text' => "An opportunity to work and upgrade oneself, as well as being involved in an organization that believes in gaining a competitive edge and giving back to the community. I'm presently expanding my solid experience in UI / UX design. I focus on using my interpersonal skills to build good user experience and create a strong interest in my employers. I hope to develop skills in motion design and my knowledge of the Web, and become an honest asset to the business. As an individual, I'm self-confident you’ll find me creative, funny and naturally passionate. I’m a forward thinker, which others may find inspiring when working as a team."),
				array('title' => 'what i do?', 'subtitle' => 'User Experience Design; Interface Design; Product Design; Branding Design; Digital Painting; Video Editing', 'text'=> "I have been working as a web interface designer since. I have a love of clean, elegant styling, and I have lots of experience in the production of CSS3 and HTML5 for modern websites. I loving creating awesome as per my clients’ need. I think user experience when I try to craft something for my clients. Making a design awesome.")
			))
		)
	);
	
	$wp_customize->add_control(
		new Parallax_One_General_Repeater(
			$wp_customize,
			'proresume_about_section_introductions',
			array(
				'label' => __('Introduce yourself'),
				'section' => 'proresume_about_section',
				'parallax_title_control' => true,
				'parallax_subtitle_control' => true,
				'parallax_text_control' => true

			)
		)
	);

	/* Signature */
	$wp_customize->add_setting(
		'proresume_about_section_signature', 
		array(
			'sanitize_callback' => 'esc_url'
		)
	);

	$wp_customize->add_control(
		new Proresume_Customize_File_Control(
			$wp_customize,
			'proresume_about_section_signature',
			array(
				'label' => __('Your signature', 'proresume'),
				'section' => 'proresume_about_section'
			)
		)
	);

	/* Attach CV */
	$wp_customize->add_setting(
		'proresume_about_section_cv', 
		array(
			'sanitize_callback' => 'esc_url'
		)
	);

	$wp_customize->add_control(
		new Proresume_Customize_File_Control(
			$wp_customize,
			'proresume_about_section_cv',
			array(
				'label' => __('Your CV', 'proresume'),
				'section' => 'proresume_about_section'
			)
		)
	);


	/****************************************/
	/* VIDEO SECTION */
	/****************************************/
	$wp_customize->add_section( 
		'proresume_video_section' , 
		array(
    'title'      => __( 'Video section', 'proresume' ),
    'priority'   => 30,
	) );

	/* Introduction */
	$wp_customize->add_setting(
		'proresume_video_section_introduction',
		array(
			'sanitize_callback' => 'esc_html',
			'default' => 'Video Introduction'
		)
	);

	$wp_customize->add_control(
		'proresume_video_section_introduction',
		array(
			'label' => __('Introduction', 'proresume'),
			'section' => 'proresume_video_section'
		)
	);

	/* Modal title */
	$wp_customize->add_setting(
		'proresume_video_section_title',
		array(
			'sanitize_callback' => 'esc_html',
			'default' => 'Video Tour'
		)
	);

	$wp_customize->add_control(
		'proresume_video_section_title',
		array(
			'label' => __('Modal Title', 'proresume'),
			'section' => 'proresume_video_section'
		)
	);

	/* Autoplay */
	$wp_customize->add_setting(
		'proresume_video_section_autoplay',
		array(
			'sanitize_callback' => 'esc_html',
			'default' => '1'
		)
	);

	$wp_customize->add_control(
		'proresume_video_section_autoplay',
		array(
			'label' => __('Is autoplay ?', 'proresume'),
			'section' => 'proresume_video_section'
			,'type' => 'checkbox'
		)
	);

	/* Background image */
	$wp_customize->add_setting(
		'proresume_video_section_background',
		array(
			'sanitize_callback' => 'esc_url',
			'default' => proresume_get_asset_url('images/video-bg.jpg')
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'proresume_video_section_background',
			array(
				'label' => __('Your background', 'proresume'),
				'section' => 'proresume_video_section',
			)
		)
	);

	/* Video links */
	$wp_customize->add_setting(
		'proresume_video_section_links',
		array(
			'sanitize_callback' => 'parallax_one_sanitize_repeater',
			'default' => json_encode(
				array(
					array('provider' => 'vimeo', 'link' => 'http://player.vimeo.com/video/118119037'),
					array('provider' => 'vimeo', 'link' => 'http://player.vimeo.com/video/118119038'),
					array('provider' => 'vimeo', 'link' => 'http://player.vimeo.com/video/118119039')
				)
			)
		)
	);

	$wp_customize->add_control(new Parallax_One_General_Repeater(
		$wp_customize,
		'proresume_video_section_links',
		array(
			'label' => __('Video links', 'proresume'),
			'section' => 'proresume_video_section',
			'parallax_link_control' => true
		)
	));


	/****************************************/
	/* CONTACT SECTION */
	/****************************************/
	$wp_customize->add_section( 
		'proresume_contact_section' , 
		array(
    'title'      => __( 'Contact section', 'proresume' ),
    'priority'   => 30,
	) );

	/* Form Contact */
	$wp_customize->add_setting(
		'proresume_contact_section_form_shortcode',
		array(
			'sanitize_callback' => 'sanitize_text_field',
			'default' => ''
		)
	);

	$wp_customize->add_control(
		'proresume_contact_section_form_shortcode',
		array(
			'label' => __('Form Contact', 'proresume'),
			'section' => 'proresume_contact_section',
			'description' => __('Place here a shortcode', 'proresume')
		)
	);

	/* Map */
	$wp_customize->add_setting(
		'proresume_contact_section_map_shortcode',
		array(
			'sanitize_callback' => 'sanitize_text_field',
			'default' => ''
		)
	);

	$wp_customize->add_control(
		'proresume_contact_section_map_shortcode',
		array(
			'label' => __('Map', 'proresume'),
			'section' => 'proresume_contact_section',
			'description' => __('Place here a shortcode', 'proresume')
		)
	);

	/* Address */
	$wp_customize->add_setting(
		'proresume_contact_section_address',
		array(
			'sanitize_callback' => 'esc_html',
			'default' => ''
		)
	);

	$wp_customize->add_control(
		'proresume_contact_section_address',
		array(
			'label' => __('Address', 'proresume'),
			'section' => 'proresume_contact_section',
			'description' => __('Your address', 'proresume')
		)
	);

	/* Phone numbers */
	$wp_customize->add_setting(
		'proresume_contact_section_phonenumbers',
		array(
			'sanitize_callback' => 'esc_html',
			'default' => ''
		)
	);

	$wp_customize->add_control(
		'proresume_contact_section_phonenumbers',
		array(
			'label' => __('Phone numbers', 'proresume'),
			'section' => 'proresume_contact_section',
			'description' => __('Your phone numbers', 'proresume')
		)
	);


	/****************************************/
	/* FOOTER SECTION */
	/****************************************/
	$wp_customize->add_section( 
		'proresume_footer_section' , 
		array(
    'title'      => __( 'Footer section', 'proresume' ),
    'priority'   => 30,
	) );

	/* Coppyright */
	$wp_customize->add_setting(
		'proresume_footer_section_copyright',
		array(
			'sanitize_callback' => 'sanitize_text_field',
			'default' => __('Proresume 2016. All rights reserved.')
		)
	);

	$wp_customize->add_control(
		'proresume_footer_section_copyright',
		array(
			'label' => __('Coppyright', 'proresume'),
			'section' => 'proresume_footer_section',
		)
	);


}

?>