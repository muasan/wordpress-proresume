<?php 

/* ======================================== */
/* Milestone custom post type */
/* ======================================== */

/* Register post type milestone */
function proresume_register_post_type_milestone() {
	$args = array(
		'label' => 'All Milestones',
		'labels' => array(
				'name' => 'Milestones',
				'singlular_name' => 'Milestone',
				'all_items' => 'All Milestones',
				'add_new_item' => 'Add New Milestone'
		),
		'description' => 'Milestones of your career',
		'public' => true,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title'),
		'taxonomies' => array('milestone_cat'),
	);

	register_post_type('milestone', $args );
}


/* Register taxonomies for Milestone post type */
function proresume_register_taxonomies_for_milestone() {

	//Add category taxonomy
	$args = array(
		'hierarchical' => true
	);
	register_taxonomy('milestone_cat', array('milestone'), $args);
}

/* Register custom fields for Milestone */
function proresume_register_custom_fields_milestone() {
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_milestone',
		'title' => 'Extra information',
		'fields' => array(
			array(
				'key' => 'field_milestone_startdate',
				'label' => 'Start Date',
				'name' => 'start_date',
				'type' => 'date_picker',
				'display_format' => 'Y/m/d'
			),
			array(
				'key' => 'field_milestone_enddate',
				'label' => 'End Date',
				'name' => 'end_date',
				'type' => 'date_picker',
				'display_format' => 'Y/m/d'
			),
			array(
				'key' => 'field_milestone_subtitle',
				'label' => 'Sub title',
				'name' => 'sub_title',
				'type' => 'text',
			),
			array(
				'key' => 'field_milestone_detail',
				'label' => 'Detail',
				'name' => 'detail',
				'type' => 'text',
			)
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'milestone',
				),
			),
		),
	));

	endif;
}



/* ======================================== */
/* Skill custom post type */
/* ======================================== */
/* Register post type skill */
function proresume_register_post_type_skill() {
	$args = array(
		'label' => 'All Skills',
		'labels' => array(
				'name' => 'Skills',
				'singlular_name' => 'Skill',
				'all_items' => 'All Skills',
				'add_new_item' => 'Add New Skill'
		),
		'description' => 'Your skills',
		'public' => true,
		'menu_icon' => 'dashicons-carrot',
		'supports' => array('title'),
	);

	register_post_type('skill', $args );
}

/* Register custom fields for Skill */
function proresume_register_custom_fields_skill	() {
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_skill',
		'title' => 'Extra information',
		'fields' => array(
			array(
				'key' => 'field_skill_ismain',
				'label' => 'Main skill?',
				'name' => 'is_main',
				'type' => 'true_false',
				'default' => true
			),
			array(
				'key' => 'field_skill_cover',
				'label' => 'How much you know',
				'name' => 'cover',
				'type' => 'number_slider',
			),
			
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'skill',
				),
			),
		),
	));

	endif;
}


/* ======================================== */
/* Work custom post type */
/* ======================================== */
/* Register post type work */
function proresume_register_post_type_work() {
	$args = array(
		'label' => 'All Works',
		'labels' => array(
				'name' => 'Works',
				'singlular_name' => 'Work',
				'all_items' => 'All Work',
				'add_new_item' => 'Add New Work'
		),
		'description' => 'Your works',
		'public' => true,
		'menu_icon' => 'dashicons-admin-site',
		'supports' => array('title', 'editor'),
	);

	register_post_type('work', $args );
}

/* Register custom fields for Work */
function proresume_register_custom_fields_work	() {
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_work',
		'title' => 'Extra information',
		'fields' => array(
			array(
				'key' => 'field_work_images',
				'label' => 'Gallery',
				'name' => 'gallery',
				'type' => 'gallery',
			),
			array(
				'key' => 'field_work_summary',
				'label' => 'Summary',
				'name' => 'summary',
				'type' => 'textarea',
			),
			array(
				'key' => 'field_work_link',
				'label' => 'Work link',
				'name' => 'link',
				'type' => 'text',
			),
			
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'work',
				),
			),
		),
	));

	endif;
}

/* Register taxonomies for Work post type */
function proresume_register_taxonomies_for_work() {

	//Add category taxonomy
	$args = array(
	);
	register_taxonomy('work_tag', array('work'), $args);
}

?>