<?php 
/**
 * All functions related to Work post type 
 */

if (!function_exists('proresume_add_css_for_work_page')){
	function proresume_add_css_for_work_page() {
		if (is_single() and get_post_type() == 'work') {
			wp_enqueue_style('bootstrap');
			wp_enqueue_style('font');
			wp_enqueue_style('animate');
			wp_enqueue_style('magnific');
			wp_enqueue_style('custom-style');
			wp_enqueue_style('responsive');
			wp_enqueue_style('symbols');
			wp_enqueue_style('slideme-css');
			wp_enqueue_style('footer');
			wp_enqueue_style('scroller');
			wp_enqueue_style('home');

		}
	}
}
?>