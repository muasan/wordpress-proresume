<?php 
if ( class_exists('WP_Customize_Control') ) :

class Proresume_Customize_File_Control extends WP_Customize_Control {
	private $options = array();
	static private $first_instance;

	public function __construct( $manager, $id, $args = array() ) {
    parent::__construct( $manager, $id, $args );
    $this->options = $args;

    if (!$this->first_instance) {
    	$this->first_instance = $this;
    	add_action('customize_controls_enqueue_scripts', array($this, 'render_scripts'));

    }
  }

  public static function register_actions() {
    add_action('customize_controls_enqueue_scripts', array($this, 'render_scripts'));

  }

  static public function render_scripts() {
  }

  public function render_content() {

  	?>
  	<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

		<label for="upload_file_<?php echo $this->id; ?>">
		    <input class="upload_file" <?php echo $this->link(); ?> data-id="<?php echo $this->id; ?>" type="text" size="36" name="ad_image" value="<?php echo $this->value(); ?>" /> 
		    <input data-id="<?php echo $this->id; ?>" class="button upload_file_button" type="button" value="Upload File" />
		    <br />Enter a URL or upload an file
		</label>

  	<?php
  }


}//end class


endif; 
?>