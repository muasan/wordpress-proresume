<?php 

add_action('wp_enqueue_scripts', 'proresume_required_css');
add_action('wp_enqueue_scripts', 'proresume_required_scripts');
add_action('customize_register', 'proresume_customize_register' );

/**
 * Add script/css for blog
 */
add_action('wp_enqueue_scripts', 'proresume_add_js_css_for_blog');

/**
 * Add settings for blog
 */
add_action( 'admin_init', 'proresume_register_settings_for_blog' );

/**
 * Add social media tags to header for Blog
 */
add_action('wp_head', 'proresume_add_social_medial_tags_to_header_for_blog');



/**
 * Add css for home page
 */
add_action('wp_enqueue_scripts', 'proresume_add_css_for_homepage');

/**
 * Add css for work page
 */
add_action('wp_enqueue_scripts', 'proresume_add_css_for_work_page');

//Admin
add_action('admin_enqueue_scripts', 'proresume_required_css_admin' );
add_action('admin_enqueue_scripts', 'proresume_required_scripts_admin');
add_action('customize_controls_enqueue_scripts', 'proresume_customizer_scripts');

//Custom post type
add_action('init', 'proresume_register_taxonomies_for_milestone');
add_action('init', 'proresume_register_post_type_milestone');
add_action('init', 'proresume_register_custom_fields_milestone');

add_action('init', 'proresume_register_post_type_skill');
add_action('init', 'proresume_register_custom_fields_skill');

add_action('init', 'proresume_register_post_type_work');
add_action('init', 'proresume_register_custom_fields_work');
add_action('init', 'proresume_register_taxonomies_for_work');

?>