<?php 

function proresume_extract_skills_by_type($query, &$main_skills, &$other_skills) {
  while ($query->have_posts()) {
    $query->next_post();
    if (get_field('is_main', $query->post->ID))
      $main_skills[] = $query->post;
    else 
      $other_skills[] = $query->post;
  }
}


?>