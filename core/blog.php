<?php 
/**
 * All functions related to blog
 */

/**
 * Add social media tags to header
 * 
 */
if (!function_exists('proresume_add_social_medial_tags_to_header_for_blog')) {
	function proresume_add_social_medial_tags_to_header_for_blog() {
		function get_first_paragraph(){
			global $post;
			
			$str = wpautop( get_the_content() );
			$str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
			$str = strip_tags($str, '<a><strong><em>');
			return esc_html($str);
		}
		the_post();
		if (is_single() and get_post_type() == 'post') {
			$metadata = array(
				array('property'=>'og:url', 'content'=> get_permalink()),
				array('property'=>'og:image ', 'content'=> get_the_post_thumbnail_url(null, array(630,1200))),
				array('property'=>'og:title', 'content'=> get_the_title()),
				array('property'=>'og:description', 'content'=> get_first_paragraph()),
				array('property'=>'fb:app_id', 'content'=>''),
			);

			foreach ($metadata as $key => $value) {
				echo "<meta property='{$value['property']}' content='{$value['content']}' />";
			}
		}
		else if ( is_page_template('page-blogs.php')) {
			$author = get_the_author();
			$metadata = array(
				array('property'=>'og:url', 'content'=> get_permalink()),
				array('property'=>'og:image ', 'content'=> get_avatar_url(get_the_author_meta("ID"), array("size"=>630))),
				array('property'=>'og:title', 'content'=> get_the_title()),
				array('property'=>'og:description', 'content'=> __("Welcome to {$author}&apos;s blog", TEXT_DOMAIN)),
				array('property'=>'fb:app_id', 'content'=>''),
			);

			foreach ($metadata as $key => $value) {
				echo "<meta property='{$value['property']}' content='{$value['content']}' />";
			}
		}


		rewind_posts();
		
	}
}

/**
 * Register settings
 */
if (!function_exists('proresume_register_settings_for_blog')) {
	function proresume_register_settings_for_blog() {
		$option_group 			= 'reading';
		$sanitize_callback	= 'esc_url';

		$option_name 				= SOCIAL_FACEBOOK_OPTION_NAME;
		register_setting($option_group, $option_name, $sanitize_callback);

		$option_name 				= SOCIAL_LINKEDIN_OPTION_NAME;
		register_setting($option_group, $option_name, $sanitize_callback);

		$option_name 				= SOCIAL_GITHUB_OPTION_NAME;
		register_setting($option_group, $option_name, $sanitize_callback);

		$option_name 				= SHORT_DESCRIPTION_ON_BLOG;
		register_setting($option_group, $option_name, $sanitize_callback);


		$print_out_callback = function ($args) {
			$current_value = get_option($args['option_name'], '');
			echo "<input value='{$current_value}' id='{$args['option_name']}' name='{$args['option_name']}' placeholder='{$args['placeholder']}'/>";
		};

		$page 		= 'reading';
		$section 	= 'default';

		$title 				= __('Facebook', TEXT_DOMAIN);
		$option_name 	= SOCIAL_FACEBOOK_OPTION_NAME;
		$args = array('option_name' => $option_name, 'placeholder'=> __('Input a link to facebook', TEXT_DOMAIN));
		add_settings_field($option_name, $title, $print_out_callback, $page, $section, $args);

		$title 				= __('Linkedin', TEXT_DOMAIN);
		$option_name 	= SOCIAL_LINKEDIN_OPTION_NAME;
		$args = array('option_name' => $option_name, 'placeholder'=> __('Input a link to linkedin', TEXT_DOMAIN));
		add_settings_field($option_name, $title, $print_out_callback, $page, $section, $args);


		$title 				= __('Github', TEXT_DOMAIN);
		$option_name 	= SOCIAL_GITHUB_OPTION_NAME;
		$args = array('option_name' => $option_name, 'placeholder'=> __('Input a link to github', TEXT_DOMAIN));
		add_settings_field($option_name, $title, $print_out_callback, $page, $section, $args);
		
	}
}


/**
 * Add js and css to blog
 */
if (!function_exists('proresume_add_js_css_for_blog')) {
	function proresume_add_js_css_for_blog() {

		if (is_single()) {
			the_post();
			if (get_post_type() == 'post') {
					wp_enqueue_style('bootstrap');
					wp_enqueue_style('font');
					wp_enqueue_style('footer');
					wp_enqueue_style('scroller');
					wp_enqueue_style('custom-style');

					wp_enqueue_style('blog-css');
					wp_enqueue_script('blog-js');
					wp_enqueue_script('prettify');

			}
			rewind_posts();

		}
		else if (is_page_template('page-blogs.php')) {
			wp_enqueue_style('bootstrap');
					wp_enqueue_style('font');
					wp_enqueue_style('footer');
					wp_enqueue_style('scroller');
					wp_enqueue_style('custom-style');

					wp_enqueue_style('blog-css');
					wp_enqueue_script('blog-js');
		}
	}
}
?>