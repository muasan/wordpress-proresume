<?php 
/**
 * This is a template to represent a sidebar for blog webpages
 * Note: PHP codes and HTML5 will be mixed. PHP codes being used for binding data
 */

the_post();

$facebook_url = get_option(SOCIAL_FACEBOOK_OPTION_NAME, '');
$linkedin_url = get_option(SOCIAL_LINKEDIN_OPTION_NAME, '');
$github_url 	= get_option(SOCIAL_GITHUB_OPTION_NAME, '');
?>

<section class='col-sm-12 col-md-3 col-lg-3 blog-author' >
	<header>
		<a href='<?php echo get_home_url(); ?>' > <h1><?php the_author();?></h1> </a>
		<div class='author-img' style='background-image:url("<?php echo get_avatar_url(get_the_author_meta("ID"), array("size"=>180))?>")'></div>
	</header>
	
	<div class='short-description'><?php the_author_meta('description')?></div>
	<div class='social'>
		<a href='<?php echo $facebook_url; ?>' class="fa fa-2x fa-facebook" href="#"><span class="sr-only">facebook</span></a>
		<a href='<?php echo $linkedin_url; ?>' class="fa fa-2x fa-linkedin" href="#"><span class="sr-only">linkedin</span></a>
		<a href='<?php echo $github_url; ?>' class="fa fa-2x fa-github" href="#"><span class="sr-only">github</span></a>
	</div>

	<?php if (is_single()) : ?>
	<?php 
		$args = array(
							'meta_key' => '_wp_page_template' ,
							'meta_value' => 'page-blogs.php'
						);	
		$pages = get_pages($args); 
		$page = count($pages) > 0 ? $pages[0] : 0;
	?>
	<?php if ($page) : ?>
		<div class='blog-home'>
			<a href='<?php echo get_permalink($page); ?>'> <i class="fa fa-home fa-2x" aria-hidden="true"></i>
 </a>
		</div>
	<?php endif; ?>
	<?php endif;?>
</section>

<?php rewind_posts(); ?>