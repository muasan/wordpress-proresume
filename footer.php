<?php 
$copyright = get_theme_mod('proresume_footer_section_copyright', __('Proresume 2016. All rights reserved.'));
if (!$copyright || $copyright === '')
	$copyright = __('Proresume 2016. All rights reserved.');
?>

<!-- Footer Section -->
<footer class="footer-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="copyright text-center">
          <p>
          	<?php echo '&copy; ' . $copyright; ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</footer><!-- End Footer Section -->

<!-- Scroll-up -->
	<div class="scroll-up">
		<a href="#home"><i class="fa fa-2x fa-arrow-circle-up"></i></a>
	</div>


<!-- hooked Javascript files -->
<?php wp_footer(); ?>

</body>
</html>