<?php 
get_header();

$sections = array(
								"home",
								"navigation",
								"about",
								"video",
								"resume",
								"skills",
								"works",
								"facts",
								"hire",
								"contact",
						);


foreach ($sections as $index => $sec) {
	get_template_part("sections/section", $sec);
}





get_footer(); 
?>